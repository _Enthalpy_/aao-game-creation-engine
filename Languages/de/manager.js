{
"trial_manager" : "Fallmanager",

"own_trials" : "Deine Fälle",
"collaborating_trials" : "Fälle, an denen Du mitarbeitest",

"independant_trials" : "Unabhängige Fälle",
"trial_sequences" : "Fallserien",

"open_editor" : "Im Editor öffnen",
"save" : "Speichern",
"confirm_delete" : "Bist Du sicher, dass Du diese element löschen möchtest? Dies kann nicht rückgängig gemacht werden.",

"case_rules" : "Ich bestätige, dass ich die <a href=\"<url>\">Fallregeln</a> gelesen habe und dass dieser Fall sie befolgen wird."
}
