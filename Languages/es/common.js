{
	"date_format" : "d/m/A",
	"time_format" : "H:m",
	
	"home" : "Inicio",
	"search" : "Juegos",
	"manager" : "Gestor",
	"forums" : "Foros",
	"help" : "Ayuda",
	"about" : "Sobre",
	
	"copyright_aao" : "Ace Attorney Online © Unas & Spparrow 2006–<year>. Se prohibe la copia del contenido de esta página sin el consentimiento del webmaster.",
	"copyright_capcom" : "Ace Attorney® y propiedades asociadas son de © Capcom, Ltd. Ace Attorney Online no se asocia a Capcom y es dirigida únicamente por fans.",
	"disclaimer_title" : "Renuncia de responsabilidad",
	"disclaimer_text" : "Esta página está a su disposición para expresar su creatividad. No es una forma de evitar comprar juegos de Ace Attorney.",
	
	"profile_judge" : "Juez"
}
